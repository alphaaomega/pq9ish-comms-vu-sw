/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PQ9ISH_H_
#define PQ9ISH_H_

#include <stdint.h>
#include <ax5043.h>
#include <aprs.h>

#define TX_AX25 (1)

typedef struct {
    uint32_t sysTick;
    float battVolt;
    float inputVolt;
} pq9ish_system_status_t;

/**
 * Error codes of the PQ9ISH. Users should return the negative of the error
 * codes. NO_ERROR is set to zero.
 */
typedef enum {
    PQ9ISH_SUCCESS = 0,                    //!< All ok!
    PQ9ISH_INVALID_PARAM,                   //!< An invalid parameter was given
    PQ9ISH_MAX_SPI_TRANSFER_ERROR,          //!< The requested SPI data transfer was larger than supported
    PQ9ISH_TIMEOUT,                         //!< A timeout occurred
    PQ9ISH_WEATHER_INFO_ALREADY_SET         //!< A weather information field has been already set
} PQ9ISH_error_t;

typedef struct {
    ax5043_conf_t         hax5043;
    aprs_conf_t           haprs;
    int                   framing;
} pq9ish_rf_conf_t;

int
pq9ish_rf_init(pq9ish_rf_conf_t *conf);

int
pq9ish_tx(pq9ish_rf_conf_t *conf, const tx_data_t *tx_data);

void
enable_pa(ax5043_conf_t *hax);


#endif /* PQ9ISH_H_ */
