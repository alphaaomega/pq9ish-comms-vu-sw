/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spi_utils.h"
#include "stm32l4xx_hal.h"
#include "main.h"
#include "utils.h"
#include <string.h>

extern SPI_HandleTypeDef hspi2;
static uint8_t __spi_tx_buf[SPI_MAX_TRASNFER_SIZE];
static uint8_t __spi_rx_buf[SPI_MAX_TRASNFER_SIZE];

static inline int
read_short_mode(uint8_t *out, uint8_t reg);

static inline int
write_short_mode(uint8_t val, uint8_t reg);

static inline int
read_long_mode(uint8_t *out, uint16_t reg, uint32_t len);

static inline int
write_long_mode(const uint8_t *val, uint16_t reg,
                uint32_t len);

/**
 * Enables/disables the SEL pin of an SPI device
 * @param dev the SPI device
 * @param enable set 1 to enable, 0 to disable
 * @return 0 on success or appropriate negative error code
 */
int
spi_sel_dev(spi_dev_t dev, uint8_t enable)
{
    /* TODO: Disable all other existing SPI devices*/
    switch (dev) {
    case SPI_DEV_AX5043:
        HAL_GPIO_WritePin(GPIO_SPI_AX5043_SEL_GPIO_Port,
                          GPIO_SPI_AX5043_SEL_Pin, (~enable & 0x1));
        break;
    default:
        /* FIXME: Return meaningful error code */
        return -1;
    }
    return 0;
}

/**
 * Reads data using the SPI
 * @param conf the AX5043 configuration handler
 * @param out a buffer to hold the read data
 * @param reg the register address to start reading from
 * @param len how many bytes to read
 * @return 0 on success or appropriate negative error code
 */
ax5043_code_t
ax5043_spi_read(uint8_t *out, uint16_t reg, uint32_t len)
{
    int ret = HAL_OK;

    if (len > SPI_MAX_TRASNFER_SIZE - 2) {
        return -AX5043_INVALID_PARAM;
    }

    spi_sel_dev(SPI_DEV_AX5043, 1);

    /* Short address mode */
    if (len == 1 && reg < 0x7F) {
        ret = read_short_mode(out, reg);
    }
    else {
        ret = read_long_mode(out, reg, len);
    }
    spi_sel_dev(SPI_DEV_AX5043, 0);
    return ret;
}

ax5043_code_t
ax5043_spi_write(uint8_t *in, uint16_t reg, uint32_t len)
{
    int ret = HAL_OK;

    if (len > SPI_MAX_TRASNFER_SIZE - 2) {
        return -AX5043_INVALID_PARAM;
    }

    spi_sel_dev(SPI_DEV_AX5043, 1);

    /* Short address mode */
    if (len == 1 && reg < 0x7F) {
        ret = write_short_mode(in[0], reg);
    }
    else {
        ret = write_long_mode(in, reg, len);
    }
    spi_sel_dev(SPI_DEV_AX5043, 0);
    return ret;
}

int
ax5043_device_select(uint8_t enable)
{
    return spi_sel_dev(SPI_DEV_AX5043, enable);
}

/**
 * Read a 8-bit register using the short access mode.
 * NOTE: All checks must be performed by the caller. As this is a static
 * fucntion there is no way to be called outside the scope of this file.
 * @param conf the AX5043 configuration handler
 * @param out pointer to store the result
 * @param reg the register to read
 * @return 0 on success or negative error code
 */
static inline int
read_short_mode(uint8_t *out, uint8_t reg)
{
    HAL_StatusTypeDef status;
    uint16_t response;
    uint16_t addr = (0x7F & reg);

    status = HAL_SPI_TransmitReceive(&hspi2, (uint8_t *) &addr,
                                     (uint8_t *) &response, sizeof(uint16_t),
                                     SPI_TIMEOUT);
    *out = (uint8_t)(response >> 8);
    return status;
}

static inline int
write_short_mode(const uint8_t val, uint8_t reg)
{
    HAL_StatusTypeDef status;
    uint16_t request = BIT(7) | (0x7F & reg) | (((uint16_t) val) << 8);

    status = HAL_SPI_Transmit(&hspi2, (uint8_t *) &request, sizeof(uint16_t),
                              SPI_TIMEOUT);
    return status;
}

static inline int
read_long_mode(uint8_t *out, uint16_t reg, uint32_t len)
{
    HAL_StatusTypeDef status;
    uint8_t mask = BIT(6) | BIT(5) | BIT(4);

    memset(__spi_tx_buf, 0, SPI_MAX_TRASNFER_SIZE);

    /* Prepare the long access header AND9347 p.5 */
    __spi_tx_buf[0] = mask | (~mask & (reg >> 8));
    __spi_tx_buf[1] = reg & 0xFF;

    status = HAL_SPI_TransmitReceive(&hspi2, __spi_tx_buf, __spi_rx_buf,
                                     len + 2, SPI_TIMEOUT);
    if (status == HAL_OK) {
        /* Skip the status response */
        memcpy(out, __spi_rx_buf + 2, len);
    }
    return status;
}

static inline int
write_long_mode(const uint8_t *val, uint16_t reg,
                uint32_t len)
{
    HAL_StatusTypeDef status;
    uint8_t mask = BIT(7) | BIT(6) | BIT(5) | BIT(4);

    memset(__spi_tx_buf, 0, SPI_MAX_TRASNFER_SIZE);

    /* Prepare the long access header AND9347 p.5 */
    __spi_tx_buf[0] = mask | (~mask & (reg >> 8));
    __spi_tx_buf[1] = reg & 0xFF;

    memcpy(__spi_tx_buf + 2, val, len);
    status = HAL_SPI_Transmit(&hspi2, __spi_tx_buf, len + 2, SPI_TIMEOUT);
    return status;
}

/**
 * Get the SPI status bits from the last SPI transaction.
 * Note that in order both SPI status bytes to be valid, an SPI read in
 * long mode should be performed prior this function call.
 * This can be performed by issuing an SPI read command with a length of at
 * least 2 bytes.
 *
 * @return the SPI status bits
 */
uint16_t
ax5043_get_last_spi_status()
{
    uint16_t status = 0x0;
    status = __spi_rx_buf[0];
    status = (status << 8) | __spi_rx_buf[1];
    return status;
}
